import 'package:flutter/material.dart';

void main() {
  runApp(WeatherPage());
}

class WeatherPage extends StatelessWidget {
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        backgroundColor: Colors.grey,
        body: ListView(
          children: <Widget>[
            Container(
              height: 660,
              width: double.infinity,
              decoration: const BoxDecoration(
                image: DecorationImage(
                    image: NetworkImage("https://i.pinimg.com/564x/0f/a7/1b/0fa71b19c85550383ca2a3f0586dd823.jpg"),
                    fit: BoxFit.cover),
              ),
              child: ListView(
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(top: 50),
                        width: double.infinity,
                        child: Text(
                          "อ.เมืองชลบุรี",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: 30,
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      Container(
                        width: double.infinity,
                        child: Text(
                          "25°",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: 80,
                            color: Colors.white,
                          ),
                        ),
                      ),
                      Container(
                        width: double.infinity,
                        child: Text(
                          "ท้องฟ้าโปร่งเป็นส่วนมาก",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: 20,
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(bottom: 20),
                        width: double.infinity,
                        child: Text(
                          "สูงสุด: 30° ต่ำสุด: 20°",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: 20,
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      Container(
                        height: 180,
                        margin: EdgeInsets.fromLTRB(20, 1, 20, 10),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Colors.black45,
                        ),
                        child: Column(
                          children: [
                            Container(
                              padding: EdgeInsets.all(15.0),
                              child: Text(
                                  'คาดว่ามีเมฆเป็นบางส่วนประมาณเวลา 20:00',
                                  style: TextStyle(
                                    fontSize: 15,
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                  )),
                            ),
                            Divider(
                              color: Colors.white,
                              height: 10,
                              indent: 20,
                              endIndent: 20,
                            ),
                            Container(
                              padding: EdgeInsets.all(10.0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: <Widget>[
                                  TempNow(),
                                  Temp20(),
                                  Temp21(),
                                  Temp22(),
                                  Temp23(),
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                      Container(
                        height: 610,
                        margin: EdgeInsets.fromLTRB(20, 1, 20, 70),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Colors.black45,
                        ),
                        child: Column(
                          children: [
                            Container(
                              padding: EdgeInsets.all(6.0),
                              child: Text(
                                  'พยากรณ์อากาศ 10 วัน',
                                  style: TextStyle(
                                    fontSize: 13,
                                    color: Colors.blueGrey,
                                    fontWeight: FontWeight.bold,
                                  )),
                            ),
                            Divider(
                              color: Colors.white,
                              height: 1,
                              indent: 20,
                              endIndent: 20,
                            ),
                            Container(
                              padding: EdgeInsets.all(3.0),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: <Widget>[
                                  Today(),
                                  Divider(
                                    color: Colors.white,
                                    height: 1,
                                    indent: 20,
                                    endIndent: 20,
                                  ),
                                  Mon(),
                                  Divider(
                                    color: Colors.white,
                                    height: 1,
                                    indent: 20,
                                    endIndent: 20,
                                  ),
                                  Tue(),
                                  Divider(
                                    color: Colors.white,
                                    height: 1,
                                    indent: 20,
                                    endIndent: 20,
                                  ),
                                  Wed(),
                                  Divider(
                                    color: Colors.white,
                                    height: 1,
                                    indent: 20,
                                    endIndent: 20,
                                  ),
                                  Thu(),
                                  Divider(
                                    color: Colors.white,
                                    height: 1,
                                    indent: 20,
                                    endIndent: 20,
                                  ),
                                  Fri(),
                                  Divider(
                                    color: Colors.white,
                                    height: 1,
                                    indent: 20,
                                    endIndent: 20,
                                  ),
                                  Sat(),
                                  Divider(
                                    color: Colors.white,
                                    height: 1,
                                    indent: 20,
                                    endIndent: 20,
                                  ),
                                  Sun(),
                                  Divider(
                                    color: Colors.white,
                                    height: 1,
                                    indent: 20,
                                    endIndent: 20,
                                  ),
                                  Mon(),
                                  Divider(
                                    color: Colors.white,
                                    height: 1,
                                    indent: 20,
                                    endIndent: 20,
                                  ),
                                  Tue(),
                                ],
                              ),
                            )
                          ],
                        ),
                      ),

                    ],
                  )
                ],
              ),
            ),

          ],
        ),

        // bottomNavigationBar: BottomNavigationBar(
        //   backgroundColor: Colors.blueGrey,
        //   items: const <BottomNavigationBarItem>[
        //     BottomNavigationBarItem(
        //       icon: Icon(Icons.map_outlined
        //         , color: Colors.white, size: 25,),
        //       label: '',
        //
        //     ),
        //     BottomNavigationBarItem(
        //       icon: Icon(Icons.circle, color: Colors.white, size: 15,),
        //       label: '',
        //     ),
        //     BottomNavigationBarItem(
        //       icon: Icon(Icons.menu, color: Colors.white, size: 25,),
        //       label: '',
        //     ),
        //   ],
        // ),


      ),
    );
  }
}

Widget TempNow(){
  return Column(
    children: <Widget>[
      Text("ตอนนี้",style: TextStyle(color:
      Colors.white, fontWeight: FontWeight.bold,),),
      IconButton(
        icon: Icon(
          Icons.cloud,
          color: Colors.white,
        ),
        onPressed: () {},
      ),
      Text("25°",style: TextStyle(color:
      Colors.white, fontWeight: FontWeight.bold,),),

    ],
  );
}

Widget Temp20(){
  return Column(
    children: <Widget>[
      Text("20",style: TextStyle(color:
      Colors.white, fontWeight: FontWeight.bold,),),
      IconButton(
        icon: Icon(
          Icons.cloud,
          color: Colors.white,
        ),
        onPressed: () {},
      ),
      Text("25°",style: TextStyle(color:
      Colors.white, fontWeight: FontWeight.bold,),),

    ],
  );
}

Widget Temp21(){
  return Column(
    children: <Widget>[
      Text("21",style: TextStyle(color:
      Colors.white, fontWeight: FontWeight.bold,),),
      IconButton(
        icon: Icon(
          Icons.nights_stay_sharp,
          color: Colors.white,
        ),
        onPressed: () {},
      ),
      Text("24°",style: TextStyle(color:
      Colors.white, fontWeight: FontWeight.bold,),),

    ],
  );
}

Widget Temp22(){
  return Column(
    children: <Widget>[
      Text("22",style: TextStyle(color:
      Colors.white, fontWeight: FontWeight.bold,),),
      IconButton(
        icon: Icon(
          Icons.nights_stay_sharp,
          color: Colors.white,
        ),
        onPressed: () {},
      ),
      Text("23°",style: TextStyle(color:
      Colors.white, fontWeight: FontWeight.bold,),),

    ],
  );
}

Widget Temp23(){
  return Column(
    children: <Widget>[
      Text("23",style: TextStyle(color:
      Colors.white, fontWeight: FontWeight.bold,),),
      IconButton(
        icon: Icon(
          Icons.nightlight_sharp,
          color: Colors.white,
        ),
        onPressed: () {},
      ),
      Text("23°",style: TextStyle(color:
      Colors.white, fontWeight: FontWeight.bold,),),

    ],
  );
}

Widget Today() {
  return ListTile(
    leading: Text("วันนี้",
      style: TextStyle(color: Colors.white,fontSize: 18, fontWeight: FontWeight.bold,),),
    title: Icon(Icons.sunny ,color: Colors.yellow),
    trailing: Text("20° ------------------- 30°",
      style: TextStyle(color: Colors.white,fontSize: 18, fontWeight: FontWeight.bold,),),

  );
}

Widget Mon() {
  return ListTile(
    leading: Text("จ.",
      style: TextStyle(color: Colors.white,fontSize: 18, fontWeight: FontWeight.bold,),),
    title: Icon(Icons.cloud ,color: Colors.white),
    trailing: Text("20° ------------------- 29°",
      style: TextStyle(color: Colors.white,fontSize: 18, fontWeight: FontWeight.bold,),),

  );
}

Widget Tue() {
  return ListTile(
    leading: Text("อ.",
      style: TextStyle(color: Colors.white,fontSize: 18, fontWeight: FontWeight.bold,),),
    title: Icon(Icons.sunny ,color: Colors.yellow),
    trailing: Text("21° ------------------- 31°",
      style: TextStyle(color: Colors.white,fontSize: 18, fontWeight: FontWeight.bold,),),

  );
}

Widget Wed() {
  return ListTile(
    leading: Text("พ.",
      style: TextStyle(color: Colors.white,fontSize: 18, fontWeight: FontWeight.bold,),),
    title: Icon(Icons.sunny ,color: Colors.yellow),
    trailing: Text("22° ------------------- 31°",
      style: TextStyle(color: Colors.white,fontSize: 18, fontWeight: FontWeight.bold,),),

  );
}

Widget Thu() {
  return ListTile(
    leading: Text("พฤ.",
      style: TextStyle(color: Colors.white,fontSize: 18, fontWeight: FontWeight.bold,),),
    title: Icon(Icons.cloudy_snowing ,color: Colors.white),
    trailing: Text("22° ------------------- 32°",
      style: TextStyle(color: Colors.white,fontSize: 18, fontWeight: FontWeight.bold,),),

  );
}

Widget Fri() {
  return ListTile(
    leading: Text("ศ.",
      style: TextStyle(color: Colors.white,fontSize: 18, fontWeight: FontWeight.bold,),),
    title: Icon(Icons.cloudy_snowing ,color: Colors.white),
    trailing: Text("22° ------------------- 27°",
      style: TextStyle(color: Colors.white,fontSize: 18, fontWeight: FontWeight.bold,),),

  );
}

Widget Sat() {
  return ListTile(
    leading: Text("ส.",
      style: TextStyle(color: Colors.white,fontSize: 18, fontWeight: FontWeight.bold,),),
    title: Icon(Icons.cloudy_snowing ,color: Colors.white),
    trailing: Text("20° ------------------- 28°",
      style: TextStyle(color: Colors.white,fontSize: 18, fontWeight: FontWeight.bold,),),

  );
}

Widget Sun() {
  return ListTile(
    leading: Text("อา.",
      style: TextStyle(color: Colors.white,fontSize: 18, fontWeight: FontWeight.bold,),),
    title: Icon(Icons.cloudy_snowing ,color: Colors.white),
    trailing: Text("23° ------------------- 32°",
      style: TextStyle(color: Colors.white,fontSize: 18, fontWeight: FontWeight.bold,),),

  );
}










